const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { tiresService } = require('../services');



const getOptions = catchAsync(async (req, res) => {
  const result = await tiresService.queryOptionsTires(req.query);
  res.send(result);
});




module.exports = {
  getOptions
};
