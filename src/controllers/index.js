module.exports.authController = require('./auth.controller');
module.exports.userController = require('./user.controller');
module.exports.motosearchController = require('./motosearch.controller');
module.exports.tiresController = require('./tires.controller');

