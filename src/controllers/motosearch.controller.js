const httpStatus = require('http-status')
const pick = require('../utils/pick')
const ApiError = require('../utils/ApiError')
const catchAsync = require('../utils/catchAsync')
const { motosearchService } = require('../services')

const getOptions = catchAsync(async (req, res) => {
  const result = await motosearchService.queryOptionsSearch(req.query)
  res.send(result)
})

const findTires = catchAsync(async (req, res) => {
  const result = await motosearchService.queryTires(req.body)
  res.send(result)
})

module.exports = {
  getOptions,
  findTires
}
