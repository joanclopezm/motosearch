const httpStatus = require('http-status')
const { Tires } = require('../models')
const ApiError = require('../utils/ApiError')

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryMotosearch = async (filter, options) => {
  const tires = await Tires.paginate(filter, options)
  return tires
}

const queryOptionsSearch = async (option) => {
  const query = {}
  if (option.Marca !== null) {
    query.Marca = new RegExp(option.Marca, 'i')
  }
  if (option.Modelo) {
    query.Modelo = new RegExp(option.Modelo, 'i')
  }
  const options = await Tires.distinct(option.name, query)
  return options
}

const queryTires = async (body) => {
  const found = await Tires.find(body)
  return found
}

module.exports = {
  queryMotosearch,
  queryOptionsSearch,
  queryTires
}
