const express = require('express')
const motosearchController = require('../../controllers/motosearch.controller')


const router = express.Router()

router
  .route('/options')
  .get(motosearchController.getOptions)
  .post(motosearchController.findTires)

module.exports = router
