const express = require('express');
const tiresController = require('../../controllers/tires.controller');


const router = express.Router();

router
  .route('/options')
  .get(tiresController.getOptions);


module.exports = router;
