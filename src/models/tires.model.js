const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const tiresSchema = mongoose.Schema({
  Nombre: { type: String },
  Referencia: { type: String },
  'Delantero/Trasero': { type: String },
  Ancho: { type: Number },
  Alto: { type: Number },
  Rin: { type: Number },
  'TT/TL': { type: String },
  'indice carga': { type: Number },
  'KM/H': { type: Number },
  'Radial/bias/diagonal': { type: String },
  Marca: { type: String },
  Modelo: { type: String },
  Cilindraje: { type: Number },
  URL: { type: String }
  }, { collection: 'llantas' }
);

// add plugin that converts mongoose to json
tiresSchema.plugin(toJSON);
tiresSchema.plugin(paginate);

const Tire = mongoose.model('Tire', tiresSchema);

module.exports = Tire;
