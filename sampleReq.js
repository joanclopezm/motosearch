const tireReq = {
  'Delantero/Trasero': 'DELANTERA', // AMBAS o  TRASERA
  Ancho: 140,
  Alto: 75,
  Rin: 17,
  'TT/TL': 'TL',
  Marca: 'Honda'
}

const motoReq = {
  Marca: 'Honda',
  Modelo: 'CG125',
  Cilindraje: 125
}
